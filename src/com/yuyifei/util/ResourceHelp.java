package com.yuyifei.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ResourceHelp {
	public static String getAndroidJarsPath() {
		String androidJarsPath = null;
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("resource/default_properties"));
		} catch (FileNotFoundException e) {
			System.err.println("resource/default_properties is non-existent !!!");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("open resource/default_properties failed!!!");
			e.printStackTrace();
		}
		
		androidJarsPath = props.getProperty("android.jars");
		
		return  androidJarsPath;
	}
}
