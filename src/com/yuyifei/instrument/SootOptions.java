package com.yuyifei.instrument;

import java.util.Collections;

import com.yuyifei.util.ResourceHelp;

import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.Transform;
import soot.options.Options;

public class SootOptions {
	public static void runSoot(ArgsOptions argsOptions) {
		String[] sootArgs = {"-process-dir", argsOptions.apkFile.getAbsolutePath()};
		
		Options.v().set_src_prec(Options.src_prec_apk);////prefer Android APK files// -src-prec apk
		
		Options.v().set_android_jars(ResourceHelp.getAndroidJarsPath());//set androidjars path
		
		Options.v().set_allow_phantom_refs(true);//skip phantom class warnning
		
		Options.v().set_exclude(Collections.singletonList("android.support.v4."));//exclude "android.support.v4." package
		
		Options.v().set_output_format(Options.output_format_dex);//output as APK, too//-f J
		//Options.v().set_output_format(Options.output_format_class);
		
		Options.v().set_output_dir(argsOptions.outDir.getAbsolutePath());
		
		//Options.v().force_overwrite();//force overwrite old output apk file
		
		// resolve the PrintStream and System soot-classes
		Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
		Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
		
		PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));
		
		soot.Main.main(sootArgs);
	}
}

