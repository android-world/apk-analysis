package com.yuyifei.instrument;

import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import soot.Body;
import soot.BodyTransformer;
import soot.PatchingChain;
import soot.Unit;
import soot.jimple.IdentityStmt;

public class MyBodyTransformer extends BodyTransformer {
	private final static Logger log = Logger.getLogger(MyBodyTransformer.class);

	@Override
	protected void internalTransform(Body body, String arg1,
			Map<String, String> arg2) {
		//instrument log information to all methods of apk file.
		instrumentLog2AllMethod(body);
	}
	
	private void instrumentLog2AllMethod(Body body) {
		String declaringClass = body.getMethod().getDeclaringClass().toString();
		if (declaringClass.contains("R$id")
				|| declaringClass.contains("R$attr")
				|| declaringClass.contains("R$layout")
				|| declaringClass.contains("R$style")
				|| declaringClass.contains("R$menu")
				|| declaringClass.contains("BuildConfig")
				|| declaringClass.contains("R$drawable")
				|| declaringClass.contains("R$string"))
			return;
		
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> i = units.snapshotIterator();
		
		//System.out.println(body.getMethod().getDeclaringClass());
		while (i.hasNext()) {
			Unit u = i.next();
			
			if (u instanceof IdentityStmt) {
				System.out.println("--------->"  + body.getMethod().toString());
				System.out.println(".........>" + u.toString());
				continue;
			}
			instrumentLog(u, body);
			return;
		}
	}

	private void instrumentLog(Unit u, Body body) {
		String methodName =  body.getMethod().toString();
		String declaringClassName = body.getMethod().getDeclaringClass().toString();
		
		log.info("{instrument] " + body.getMethod().getName());
		
		//InstrumentMethod.instrumentSystemOutPrintln(u, body, "跟踪调试信息 余奕飞44444：" + methodName);
		
		InstrumentMethod.instrumentSystemOutPrintln(u, body, "跟踪调试信息--->" 
																+ "declaringClassName:" + declaringClassName
																+ "---->"
																+ "methodName:" + methodName);
	}

}
