package com.yuyifei.instrument;

import java.io.File;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Main {
	private final static Logger log = Logger.getLogger(Test.class);
	static {//loag property configure file for log4j
		PropertyConfigurator.configure("resource/log4j.properties");
	}
	
	private static Options options;
    static {
        options = new Options();
        options.addOption("h", false, "help doc.");
        options.addOption("s", true, "apk file path");
        options.addOption("o", true, "output dir");
    }
    
	public static void main(String[] args) {
		log.info("---------------- main begin ----------------");
		//args parser
		BasicParser parser = new BasicParser();
		CommandLine cl = null;
		try {
			cl = parser.parse(options, args);
		} catch (ParseException e) {
			log.error("args parser error!!!");
			e.printStackTrace();
		}
		
		if (cl.hasOption('h')) {
			usage();
		}
		
		// new a ArgsOptions to save arguments
		ArgsOptions argsOptions = new ArgsOptions();
		
		if (cl.hasOption('s')) {
			argsOptions.apkFile = new File(cl.getOptionValue('s'));
		}
		if (null == argsOptions.apkFile) {
			usage();
		}
		
		if (cl.hasOption('o')) {
			argsOptions.outDir = new File(cl.getOptionValue('o'));
		}
		if (null == argsOptions.outDir) {
			usage();
		}
		
		//if outDir have a apk file that named as same as 
		//the apk we want to analysis, we need remove it first.
		File[] outFile = argsOptions.outDir.listFiles();
		log.info(outFile.length);
		for (int i=0; i<outFile.length; ++i) {
			if (outFile[i].getName().equals(argsOptions.apkFile.getName())) {
				log.info("apkFile exist, remove it first !!!");
				outFile[i].delete();
			}
		}
		
		//run soot
		SootOptions.runSoot(argsOptions);
	}

    private static void usage() {
        HelpFormatter help = new HelpFormatter();
        help.printHelp("-s <dex file path> -o <output dir>", options);
        System.exit(-1);
    }
}	
